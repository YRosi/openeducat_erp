# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenEduCat Inc
#    Copyright (C) 2009-TODAY OpenEduCat Inc(<http://www.openeducat.org>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


YES_NOT = [
    ('y', 'Yes'),
    ('n', 'No')
]

TURN = [
    ('m', 'Morning'),
    ('e', 'Evening'),
    ('n', 'Night'),
    ('me', 'Morning - Evening'),
    ('mn', 'Morning - Night'),
    ('en', 'Evening - Night'),
    ('men', 'Morning - Evening - Night')
]

class OpStudentCourse(models.Model):
    _name = "op.student.course"
    _description = "Student Course Details"
    _inherit = "mail.thread"
    _rec_name = 'student_id'

    student_id = fields.Many2one('op.student', 'Student', ondelete="cascade", tracking=True)
    course_id = fields.Many2one('op.course', 'Course', required=True, tracking=True)
    batch_id = fields.Many2one('op.batch', 'Batch', required=True, tracking=True)
    roll_number = fields.Char('Roll Number', tracking=True)
    subject_ids = fields.Many2many('op.subject', string='Subjects')
    academic_years_id = fields.Many2one('op.academic.year', 'Academic Year')
    academic_term_id = fields.Many2one('op.academic.term', 'Terms')
    state = fields.Selection([('running', 'Running'),
                              ('finished', 'Finished')],
                             string="Status", default="running")

    _sql_constraints = [
        ('unique_name_roll_number_id',
         'unique(roll_number,course_id,batch_id,student_id)',
         'Roll Number & Student must be unique per Batch!'),
        ('unique_name_roll_number_course_id',
         'unique(roll_number,course_id,batch_id)',
         'Roll Number must be unique per Batch!'),
        ('unique_name_roll_number_student_id',
         'unique(student_id,course_id,batch_id)',
         'Student must be unique per Batch!'),
    ]

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Student Course Details'),
            'template': '/openeducat_core/static/xls/op_student_course.xls'
        }]

class OpStudent(models.Model):
    _name = "op.student"
    _description = "Student"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _inherits = {"res.partner": "partner_id"}

    def _set_default_country_id(self):
        return self.env.ref('base.ve').id

    country_id = fields.Many2one('res.country', default=_set_default_country_id)
    first_name = fields.Char(size=128, translate=True)
    middle_name = fields.Char(size=128, translate=True)
    last_name = fields.Char(size=128, translate=True)
    second_last_name = fields.Char(size=128, translate=True)
    birth_date = fields.Date()
    mobile = fields.Char(size=15, translate=True)
    email = fields.Char(size=128, translate=True)

    headquarters = fields.Selection([
        ('FLR', 'La Floresta'),    
        ('ALT', 'Altagracia'),
        ('URB', 'La Urbina'),
        ('GCY', 'La Guaira - Carayaca')
    ], 'Headquarters', required=True)

    registration_date = fields.Date()

    pnf = fields.Selection([
        ('PA', 'PNF en Administración'),    
        ('PCP', 'PNF en Contaduría Pública'),
        ('PDL', 'PNF en Distribución y Logística'),
        ('PE', 'PNF en Educación Especial'),
        ('PINF', 'PNF en Ingeniería Informática'),
        ('PT', 'PNF en Turismo')
    ], 'PNF', required=True)

    blood_group = fields.Selection([
        ('A+', 'A+ve'),
        ('B+', 'B+ve'),
        ('O+', 'O+ve'),
        ('AB+', 'AB+ve'),
        ('A-', 'A-ve'),
        ('B-', 'B-ve'),
        ('O-', 'O-ve'),
        ('AB-', 'AB-ve')
    ], string='Blood Group')

    gender = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ('h', 'Hermaphrodite')
    ], 'Gender', required=True)

    id_number_pdf = fields.Selection(YES_NOT, required=True, default='n')
    id_number_pdf_binary = fields.Binary()
    passport_type_photo = fields.Selection(YES_NOT, required=True, default='n')
    passport_type_photo_binary = fields.Binary()
    birth_certificate = fields.Selection(YES_NOT, required=True, default='n')
    birth_certificate_binary = fields.Binary()
    high_school_diploma = fields.Selection(YES_NOT, required=True, default='n')
    high_school_diploma_binary = fields.Binary()
    certificate_note = fields.Selection(YES_NOT, required=True, default='n')
    certificate_note_binary = fields.Binary()
    opsu = fields.Selection(YES_NOT, required=True, default='n')
    opsu_binary = fields.Binary()
    rif = fields.Selection(YES_NOT, required=True, default='n')
    rif_binary = fields.Binary()

    nationality = fields.Many2one('res.country')
    emergency_contact = fields.Many2one('res.partner')
    visa_info = fields.Char(size=64)
    id_number = fields.Char('Identity Card', size=64)
    partner_id = fields.Many2one('res.partner', 'Partner',
                                 required=True, ondelete="cascade")
    user_id = fields.Many2one('res.users', 'User', ondelete="cascade")
    gr_no = fields.Char("GR Number", size=20)
    category_id = fields.Many2one('op.category', 'Category')
    course_detail_ids = fields.One2many('op.student.course', 'student_id',
                                        'Course Details',
                                        tracking=True)
    turn = fields.Selection(TURN)
    active = fields.Boolean(default=True)

    _sql_constraints = [(
        'unique_gr_no',
        'unique(gr_no)',
        'GR Number must be unique per student!'
    )]

    @api.onchange('first_name', 'middle_name', 'last_name', 'second_last_name')
    def _onchange_name(self):
        self.name = f'{self.first_name} {self.last_name}'
        if self.middle_name and not self.second_last_name:
            self.name = f'{self.first_name} {self.middle_name} {self.last_name}'
        elif self.second_last_name and not self.middle_name:
            self.name = f'{self.first_name} {self.last_name} {self.second_last_name}'
        elif self.second_last_name and self.middle_name:
            self.name = f'{self.first_name} {self.middle_name} {self.last_name} {self.second_last_name}'

    @api.onchange('state_id')
    def clear_state(self):
        """Clear fields of municipality and parish when changing the state"""
        for rec in self:
            rec.municipality_id = False
            rec.parish_id = False

    @api.onchange('municipality_id')
    def clear_municipality(self):
        """Clear fields of parish when changing the municipality"""
        for rec in self:
            rec.parish_id = False

    @api.constrains('birth_date')
    def _check_birthdate(self):
        for record in self:
            if record.birth_date > fields.Date.today():
                raise ValidationError(_(
                    "Birth Date can't be greater than current date!"))

    @api.model
    def get_import_templates(self):
        return [{
            'label': _('Import Template for Students'),
            'template': '/openeducat_core/static/xls/op_student.xls'
        }]

    def create_student_user(self):
        user_group = self.env.ref("base.group_portal") or False
        users_res = self.env['res.users']
        for record in self:
            if not record.user_id:
                user_id = users_res.create({
                    'name': record.name,
                    'partner_id': record.partner_id.id,
                    'login': record.email,
                    'groups_id': user_group,
                    'is_student': True,
                    'tz': self._context.get('tz'),
                })
                record.user_id = user_id
