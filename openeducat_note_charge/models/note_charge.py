from odoo import api, fields, models


class NoteCharge(models.Model):
    _name = "note.charge"
    # _rec_name = ""
    # _order = ""
    _description = """Carga de Notas"""

    faculty_id = fields.Many2one("op.faculty", "Profesor", required=True)
    student_id = fields.Many2one("op.student", "Estudiante", required=True)
    course_id = fields.Many2one("op.course", "PNF", required=True)
    subject_id = fields.Many2one("op.subject", "UC", required=True)
    note = fields.Float("Nota")
